const TAG = __filename;
const PATH = '/cli';

const fs = require('fs');
const path = require('path');
const router = require('express').Router();
const auth = require("../lib/auth/cli");
const logs = require("../lib/db/logs.js");
const appDB = require("../lib/db/apps");

const logsDir = path.join(__dirname, "..", "logs");

//Auth Middleware
router.use((req, res, next) => {
    try {
        let token = req.headers['auth-token'];
        if(!token) return res.sendStatus(401);
        auth.verify(token).then((valid) => {
            if(!valid) return res.sendStatus(401);
            else next();
        }).catch((err) => {
            _LOG(true, err);
            logs.addLog(err);
            res.sendStatus(500);
        });
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
});

//Info
router.get("/info", (req, res) => {
    try {
        res.status(200).json(appDB.getApps());
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
});

//Get Ops
router.get('/ops', (req, res) => {
    try {
        res.status(200).json(appDB.getAllOps());
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
});

//Get Logs List
router.get('/logs', (req, res) => {
    try {
        res.status(200).json(logs.getLogs());
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
});

//Get Log
router.get('/logs/:id', (req, res) => {
    try {
        let id = req.param('id');
        if(!id) return res.sendStatus(400);
        let log = logs.getLog(id);
        if(!log) res.sendStatus(404);
        res.status(200).json(log);        
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }    
});

//Get Log Files List
router.get('/log-files', (req, res) => {
    try {
        fs.readdir(logsDir, (err, files) => {
            if(err) return res.sendStatus(500);
            res.status(200).json(files);
        });
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
});

//Get Log File
router.get('/log-files/:id', (req, res) => {
    try {
        let id = req.param('id');
        if(!id) return res.sendStatus(400);
        let filePath = path.join(logsDir, id);
        fs.exists(filePath, (exists) => {
            if(!exists) return res.sendStatus(404);
            res.sendFile(filePath);
        });
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
});

module.exports = function(express) {
    express.use(PATH, router);
};