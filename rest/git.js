const TAG = __filename;
const PATH = '/git-hook';
const router = require('express').Router();
const appDB = require('../lib/db/apps');
const logDB = require('../lib/db/logs');
const handler = require('../lib/git/handler');
const runner = require('../lib/op/runner');

router.all('/:name', async function(req, res) {
    const log = {
        TAG: TAG,
        ERROR: false
    };
    try {
        const name = req.params.name;
        const app = appDB.getApp(name);
        if(!app) {
            log.MSG = "App not found : " + name;
            logDB.addLog(log);
            return res.sendStatus(400);
        }
        const gitInfo = await handler.process(app, req);
        switch(gitInfo.kind) {
            case 'push':
                log.MSG = "GIT HOOK :: PUSH :: " + JSON.stringify(app);
                runner.update(app);
                break;
            default:
                log.MSG = "GIT HOOK :: UNKNOWN :: " + JSON.stringify(app);
        }
        logDB.addLog(log);
    } catch (err) {
        res.sendStatus(500);

        let json = JSON.stringify(err);
        let msg = err.message ? err.message : '';
        let stack = err && err.stack ? err.stack : '';

        _LOG(true, err, json, msg, stack);

        log.ERROR = true;
        log.ERRORJSON = json;
        log.ERRORMSG = msg;
        log.ERRORSTACK = stack;
        logDB.addLog(log);
    }
});

module.exports = function(express) {
    express.use(PATH, router);
};