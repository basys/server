const TAG = __filename;
const PATH = '/app';
const router = require('express').Router();
const auth = require("../lib/auth/cli");
const apps = require("../lib/db/apps");
const logs = require("../lib/db/logs");

//Auth Middleware
router.use((req, res, next) => {
    try {
        let token = req.headers['auth-token'];
        if(!token) return res.sendStatus(401);
        auth.verify(token).then((valid) => {
            if(!valid) return res.sendStatus(401);
            else next();
        }).catch((err) => {
            _LOG(true, err);
            logs.addLog(err);
            res.sendStatus(500);
        });
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
});

//App
//Get App Info
router.get("/:id", (req, res) => {
    try {
        let id = req.params.id;
        if(!id) return req.sendStatus(400);
        let app = apps.getApp(id);
        if(!app) return res.sendStatus(404);
        res.status(200).json(app);
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
    //Delete App
}).delete("/:id", (req, res) => {
    try {
        let id = req.params.id;
        if(!id) return req.sendStatus(400);
        let app = apps.getApp(id);
        if(!app) return req.sendStatus(404);
        apps.deleteApp(app.name);
        res.sendStatus(200);
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
});

//Apps
//Get all Apps' info
router.get("/get/all", (req, res) => {
    try {
        res.status(200).json(apps.getApps());
    } catch (err) {
        _LOG(true, err);
        logs.addLog(err);
        res.sendStatus(500);
    }
});

module.exports = function(express) {
    express.use(PATH, router);
};