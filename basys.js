//Require Logger
require('./lib/util/log')('Server');
global._LOG = _LOG;

//Setup Constants
const NAME = "BaSYS - Server";
const TAG = __filename;
const PORT = 44000;

//Require Modules
const fs = require('fs');
const path = require('path');
const cors = require("cors");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const express = require("express");
const socketio = require('socket.io');
const app = express();

//Server Instances
var server;
var io;

//Setup
function setup(cb) {
    //Init Dirs
    let keysDir = path.join(__dirname, "keys");
    let scriptsDir = path.join(__dirname, "scripts");
    if(!fs.existsSync(keysDir)) fs.mkdirSync(keysDir);
    if(!fs.existsSync(scriptsDir)) fs.mkdirSync(scriptsDir);
    
    //Generate Keys if they do not exist
    require("./lib/auth/keys");

    //Start Express Server
    server = app.listen(PORT, () => {
        let props = server.address();
        props.NAME = NAME;
        _LOG(false, TAG, "Server Started", JSON.stringify(props));
    });

    //Start Socket.io Server
    io = socketio(server, {
        path: '/ws',
        pingInterval: 15000,
        pingTimeout: 10000
    });

    //Setup WS
    require('./ws/cli').attach(io);

    //Setup Express
    app.use(cors());
    app.options('*', cors());
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: false
    }));    
    require('./rest/cli')(app);
    require('./rest/app')(app);
    require('./rest/git')(app);
};

//Setup
setup();