const TAG = __filename;
const NAMESPACE = "/cli";

const uuid = require('uuid');
const to = require('timeout-callback');
const auth = require("../lib/auth/cli");
const apps = require("../lib/db/apps");
const logs = require("../lib/db/logs");
const runner = require('../lib/op/runner');
const clients = require('../lib/util/map')();
const listeners = require('../lib/util/map')();

var router;

//Util functions
const ackh = function(ack, data) {
    ack(data instanceof Object ? JSON.stringify(data) : data);
};

const addListener = function(op, socket) {
    let list = listeners.get(op.oid);
    if(!list) list = [];
    list.push(socket);
    listeners.set(op.oid, list);
};

const removeListener = function(op, socket) {
    let list = listeners.get(op.oid);
    if(!list) return;
    let index = list.indexOf(socket);
    if(index < 0) return;
    list.splice(index, 1);
};

//Modules
//Setup new client
const initClient = function(socket) {
    socket.sid = null;

    socket.on('disconnect', () => {
        if(!socket.sid) return;
        clients.delete(socket.sid);
    });

    socket.on('authenticate', async (data, ack) => {
        if(socket.sid) {
            ackh(ack, {
                CODE: 200,
                MESSAGE: 'Success'
            });
        } else {
            try {
                let json = JSON.parse(data);
                let token = json['auth-token'];
                if(!token) {
                    ackh(ack, {
                        CODE: 400,
                        MESSAGE: 'Missing auth-token'
                    });
                    socket.disconnect();
                    return;
                } else {
                    if(!(await auth.verify(token))) {
                        ackh(ack, {
                            CODE: 401,
                            MESSAGE: 'Authentication failure'
                        });
                        socket.disconnect();
                    } else {
                        socket.sid = uuid();
                        socket.timestamp = Date.now();
                        clients.set(socket.sid, socket);
                        ackh(ack, {
                            CODE: 200,
                            MESSAGE: 'Success'
                        });
                    }
                }
            } catch (err) {
                _LOG(true, err);
                logs.addLog(err);
                ackh(ack, {
                    CODE: 500,
                    MESSAGE: err.message ? err.message : 'Error',
                    ERR: err
                });
                socket.disconnect();
            }
        }
    });

    setTimeout(() => {
        if(socket.sid) return;
        socket.disconnect();
    }, 10000);
};

//Handle client requests
const handleRequests = function(socket) {
    socket.on('app-add', async (data, ack) => {
        try {
            let app = JSON.parse(data);
            let op = runner.genOp('ADD', app);
            if(!app) {
                ackh(ack, {
                    CODE: 400,
                    MESSAGE: 'Missing app details'
                });
            } else if(apps.getApp(app.name)) {
                ackh(ack, {
                    CODE: 409,
                    MESSAGE: 'App with this name already exists'
                });
            } else {
                addListener(op, socket);
                let logFilePath = await runner.add(app, op);
                removeListener(op, socket);
                apps.addApp(app);
                ackh(ack, {
                    CODE: 200,
                    MESSAGE: 'Success',
                    LOG_PATH: logFilePath
                });
            }
        } catch (err) {
            let log = err.LOG_PATH ? err.LOG_PATH : '';            
            _LOG(true, err);
            logs.addLog(err);
            ackh(ack, {
                CODE: 500,
                MESSAGE: err.message ? err.message : 'Error',
                ERR: err,
                LOG_PATH: log
            });
        }
    });

    socket.on('app-update', (data, ack) => {
        
    });
};

//Export
module.exports.attach = function(io) {
    //Create Router
    router = io.of(NAMESPACE);

    //Setup
    router.on('connection', (socket) => {
        initClient(socket);
        handleRequests(socket);
    });

    /*
     * Type
     * 0 - Normal
     * 1 - Warning
     * 2 - Error
     * 3 - Input
     * 4 - Special
     * 5 - Title
     */
    router.opLog = function(op, type, message) {
        if(!op) return;
        let list = listeners.get(op.oid);
        if(!list) return;
        list.forEach(socket => {
            socket.emit('opLog', JSON.stringify({
                OP: op,
                TYPE: type,
                MESSAGE: message
            }));
        });
    };

    module.exports.router = router;
};