const fs = require('fs');
const path = require('path');
const keysPath = path.join(__dirname, "..", "..", "keys", "cli.json");
const rsa = require('../util/rsa');

module.exports = {
    addKey: function(key) {
        return new Promise((resolve, reject) => {
            this.getKeys().then((keys) => {
                keys.push(key);
                fs.writeFileSync(keysPath, keys);
                resolve(key);
            }).catch((e) => {
                reject(e);
            });
        });
    },

    removeKey: function(key) {
        return new Promise((resolve, reject) => {
            this.getKeys().then((keys) => {
                for(let i=0; i<keys.length; ++i) {
                    if(keys[i] == key) {
                        keys.splice(i, 1);
                        fs.writeFileSync(keysPath, keys);
                        resolve(true);
                        return;
                    }
                }
                resolve(false);
            }).catch((e) => {
                reject(e);
            });
        });
    },

    hasKey: function(key) {
        return new Promise((resolve, reject) => {
            this.getKeys().then((keys) => {
                for(let i=0; i<keys.length; ++i) {
                    if(keys[i] == key) {
                        resolve(true);
                        return;
                    }
                }
                resolve(false);
            }).catch((e) => {
                reject(e);
            });
        });
    },   

    getKeys: function() {
        return new Promise((resolve, reject) => {
            try {
                let json = JSON.parse(fs.readFileSync(keysPath));
                resolve(json);
            } catch (e) {
                reject(e);
            }
        });
    },

    verify: function(token) {
        return new Promise((resolve, reject) => {
            this.getKeys().then((keys) => {
                for(let i=0; i<keys.length; ++i) {
                    if(rsa.verify(keys[i], 'CLI-TOKEN', token)) {
                        resolve(true);
                        return;
                    }
                }
                resolve(false);
            }).catch((err) => {
                reject(err);
            });
        });
    }
};