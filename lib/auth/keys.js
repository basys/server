/*
 * Keys Config
 */
const fs = require('fs');
const path = require('path');
const rsa = require("../util/rsa");
const keysDir = path.join(__dirname, "..", "..", "keys");
const keysPath = path.join(keysDir, "keys.json");
let keys;

//Get Keys else create new ones 
if(!fs.existsSync(keysDir)) {
    fs.mkdirSync(keysDir);
}
if(fs.existsSync(keysPath)) {
    keys = JSON.parse(fs.readFileSync(keysPath));
} else {
    _LOG(false, "Generating keys", "This may take a while...");
    fs.writeFileSync(keysPath, JSON.stringify(rsa.generateKeyPair()));
    keys = JSON.parse(fs.readFileSync(keysPath));
}

exports = module.exports = {
    getKeys: function() {        
        return keys;
    }
};