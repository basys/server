const gitlab = require('./gitlab');

module.exports = {
    process: async function(app, req) {
        return new Promise(async (resolve, reject) => {
            try {
                let service = app.gitconfig ? app.gitconfig.service : '';
                switch(service.toLowerCase()) {
                    case 'gitlab':
                        resolve(await gitlab.process(app.gitconfig, req));
                        break;
                    default: reject(new Error('Could not find the mentioned service to handle this webhook :: Service Name : ' + service));
                }
            } catch (err) {
                reject(err);
            }
        });
    }
};