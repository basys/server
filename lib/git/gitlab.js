module.exports = {
    process: function(config, req) {
        return new Promise((resolve, reject) => {
            try {
                if(config.token) {
                    const token = req.headers['x-gitlab-token'];
                    if(token != config.token) {
                        return reject(new Error('Gitlab token mismatch :: Token : ' + config.token + " :: Received : " + token));
                    }
                }
                const json = req.body;
                if(config.projectid != json.project_id) {
                    return reject(new Error('Project ID mismatch :: ProjectID : ' + config.projectid + " :: Received : " + json.project_id));
                }
                const info = {
                    kind: json.object_kind
                };
                resolve(info);
            } catch (err) {
                reject(err);
            }
        });
    }
};