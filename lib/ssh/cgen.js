module.exports = {
    gen: function(cmd, input, ignoreFailure) {
        if(!cmd || cmd.length==0) throw Error('Invalid command passed');
        return {
            cmd: cmd,
            
            in: input ? input : null,

            ignoreFailure: ignoreFailure==true,

            setCmd(cmd) {
                this.cmd = cmd;
                return this;
            },

            setIn(input) {
                this.in = input;
                return this;
            },

            setIgnoreFailure(ignoreFailure) {
                this.ignoreFailure = ignoreFailure;
                return this;
            }
        };
    }
};