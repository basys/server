const SSH = require('simple-ssh');
const FSSH = require('node-ssh');
const cgen = require("./cgen");
const prt = require('../util/print');
const logger = require('../util/logger');

const rootDirName = "basys";
const rootDir = "~/" + rootDirName;
const startCommands = [
    //Update repos
    cgen.gen("sudo apt update -y"),
    cgen.gen("sudo apt-get update -y"),
    cgen.gen("sudo apt-get upgrade -y"),
    cgen.gen("mkdir -p " + rootDir)
];
const endCommands = [
    cgen.gen("exit 0")
];

module.exports = function(opName, serverConfig, options) {
    //Start Logger
    const log = logger(opName);
    
    const pushLog = function(type, msg) {
        msg = new Date() + " :: " + msg;
        if(options.op && options.router && options.router.opLog) options.router.opLog(options.op, type, msg);
        if(options.stream) console.log(msg);
        log.write(msg);
    };

    //Vars
    const queue = [];
    let msg = "unknown";

    //Execute commands
    function execCommands(commands) {
        return new Promise((resolve, reject) => {
            let cmdCount = commands.length;
            let ssh = new SSH({
                host: serverConfig.host,
                port: serverConfig.port,
                user: serverConfig.username,
                pass: serverConfig.password
            });
            for(let i=0; i<commands.length; ++i) {
                let command = commands[i];
                ssh.exec(command.cmd, {
                    out: function(stream) {
                        msg = 'remote :: out :: ' + stream;
                        pushLog(0, msg);
                    },
                    err: function(stream) {
                        msg = 'remote :: err :: ' + stream;
                        pushLog(2, msg);
                    },
                    exit: function(code) {
                        cmdCount--;
                        
                        //Log Response
                        msg = 'local :: command executed : ' + command.cmd + ' :: exit code : ' + code + (cmdCount>0 ? ' :: next : ' + commands[i+1].cmd : "");
                        prt.in({}, msg);
                        pushLog(3, '\n' + msg);
    
                        if((code==0 || code==NaN) && cmdCount==0) {
                            resolve();
                        } else if(code!=0 && code!=NaN) {
                            //Log
                            msg = 'local :: command execution failed :: ' + command.cmd;
                            prt.e({}, msg);
                            pushLog(2, '\n' + msg);
                            
                            if(command.ignoreFailure) {
                                //Log
                                msg = "local :: continuing execution";
                                prt.w({}, msg);
                                pushLog(1, '\n' + msg);
                            } else if(!options.ignore) {
                                //Stop ssh exec
                                ssh.end();
                                //Send Msg
                                reject(Error(msg));
                                //Stop Exec
                                return false;
                            }
                        }
                        return true;
                    },
                    pty: true
                });
            };
            //Execute commands
            ssh.start({
                success: function() {
                    msg = "local :: connected to server";
                    prt.in({}, msg);
                    pushLog(3, '\n' + msg);
                },
                fail: function(err) {
                    reject(err);
                }
            });
        });
    };
    
    //Put File
    function execPutFile(srcPath, destPath) {
        return new Promise((resolve, reject) => {
            let ssh = new FSSH();
            ssh.connect({
                host: serverConfig.host,
                port: serverConfig.port,
                username: serverConfig.username,
                password: serverConfig.password
            }).then(() => {
                try {
                    ssh.putFile(srcPath, destPath).then(() => {
                        msg = 'local :: putFile :: src : ' + srcPath + ' :: dest : ' + destPath + ' :: exit code : 0';
                        prt.in({}, msg);
                        pushLog(3, '\n' + msg);
                        resolve();
                    }, (err) => {
                        reject(err);
                    });
                } catch (err) {
                    reject(err);
                }
            });
        });
    };

    //Put File
    function execGetFile(srcPath, destPath) {
        return new Promise((resolve, reject) => {
            let ssh = new FSSH();
            ssh.connect({
                host: serverConfig.host,
                port: serverConfig.port,
                username: serverConfig.username,
                password: serverConfig.password
            }).then(() => {
                try {
                    ssh.getFile(srcPath, destPath).then(() => {
                        msg = 'local :: getFile :: src : ' + srcPath + ' :: dest : ' + destPath + ' :: exit code : 0';
                        prt.in({}, msg);
                        pushLog(3, '\n' + msg);
                        resolve();
                    }, (err) => {
                        reject(err);
                    });
                } catch (err) {
                    reject(err);
                }
            });
        });
    };
    
    //Start Execution
    function exec() {
        return new Promise(async (resolve, reject) => {
            log.start();
            try {
                for(let i=0; i<queue.length; ++i) {
                    msg = 'local :: executing phase ' + (i + 1) + ' of ' + queue.length;
                    prt.sp({}, msg);
                    pushLog(4, '\n' + msg);
                    
                    let cmd = queue[i];
                    switch(cmd.TYPE) {
                        case 'COMMANDS':
                            await execCommands(cmd.COMMANDS);
                            break;
                        case 'PUTFILE':
                            await execPutFile(cmd.SRC, cmd.DEST);
                            break;
                        case 'GETFILE':
                            await execGetFile(cmd.SRC, cmd.DEST);
                            break;
                        default: throw Error('local :: unknown queue command type encountered');
                    }
                }
                resolve(await log.stop());
            } catch (err) {
                pushLog(2, 'ERROR: ' + err.message + (err.stack ? 'STACK: ' + err.stack : '') + '\nJSON: ' + JSON.stringify(err));
                err.LOG_PATH = await log.stop();
                reject(err);
            }
        });
    };

    return {
        rootDir: rootDir,
        rootDirName: rootDirName,
        getUserHome: function(username) {
            return (username=='root' ? "/" : '/home/') + username;
        },
        
        startCommands: startCommands,
        endCommands: endCommands,
        queueCommands: function(commands) {
            queue.push({
                TYPE: 'COMMANDS',
                COMMANDS: commands
            });
            return this;
        },
        queuePutFileCommand: function(src, dest) {
            queue.push({
                TYPE: 'PUTFILE',
                SRC: src,
                DEST: dest
            });
            return this;
        },
        queueGetFileCommand: function(src, dest) {
            queue.push({
                TYPE: 'GETFILE',
                SRC: src,
                DEST: dest
            });
            return this;
        },

        exec: exec
    };
}