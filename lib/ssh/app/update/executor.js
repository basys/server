const OPERATION = 'app-setup';

const fs = require('fs');
const path = require('path');
const cgen = require('../../cgen');
const runner = require("../../runner");

const exec = function(app, appConfig, options) {
    return new Promise(async (resolve, reject) => {
        const scriptsDir = path.join(__dirname, "..", "..", "..", "..", "scripts");

        const install = app.install;
        const scripts = app.scripts;
    
        const run = runner(OPERATION, appConfig, options);
    
        //Remote Script Paths
        const rBasysUpdate = app.dir + "/" + scripts.basysupdate;
        const rPreScript = app.dir + "/" + scripts.prescript;
        const rScript = app.dir + "/" + scripts.script;
        const rPostScript = app.dir + "/" + scripts.postscript;
    
        //Exec
        run.queueCommands(
            run.startCommands
            .concat(cgen.gen("mkdir -p " + app.dir))
            .concat(require("../../install/git").installCommands)
            .concat(run.endCommands)
        );

        //Add install scripts
        if(install) {
            for(let i=0; i<install.length; ++i) {
                let scriptPath = path.join(scriptsDir, install[i]);
                let rScriptPath = app.dir + "/" + install[i];

                if(!fs.existsSync(scriptPath)) return reject(Error(install[i] + ' : Install script not found'));

                if(install[i].endsWith('.js')) {
                    run.queueCommands(require(scriptPath).installCommands);
                } else if(install[i].endsWith('.sh')) {
                    run
                    .queuePutFileCommand(scriptPath, rScriptPath)
                    .queueCommands([
                            cgen.gen("chmod +x " + rScriptPath),
                            cgen.gen("echo 'Executing Script" + install[i] + "'"),
                            cgen.gen(rScriptPath),
                            cgen.gen('rm -f ' + rScriptPath)
                        ]
                        .concat(run.endCommands)
                    );
                }
            }
        }

        run.queuePutFileCommand(path.join(scriptsDir, scripts.basysupdate), rBasysUpdate)
        .queueCommands([
                cgen.gen("chmod +x " + rBasysUpdate),
                cgen.gen("echo 'Executing BaSYS Update Script'"),
                cgen.gen(rBasysUpdate),
                cgen.gen('rm -f ' + rBasysUpdate)
            ]
            .concat(run.endCommands)
        );
        if(scripts.prescript) {
            run
            .queuePutFileCommand(path.join(scriptsDir, scripts.prescript), rPreScript)
            .queueCommands([
                    cgen.gen("chmod +x " + rPreScript),
                    cgen.gen("echo 'Executing Prescript'"),
                    cgen.gen(rPreScript),
                    cgen.gen('rm -f ' + rPreScript)
                ]
                .concat(run.endCommands)
            );
        }
        if(scripts.script) {
            run
            .queuePutFileCommand(path.join(scriptsDir, scripts.script), rScript)
            .queueCommands([
                    cgen.gen("chmod +x " + rScript),
                    cgen.gen("echo 'Executing Script'"),
                    cgen.gen(rScript),
                    cgen.gen('rm -f ' + rScript)
                ]
                .concat(run.endCommands)
            );
        }
        if(scripts.postscript) {
            run
            .queuePutFileCommand(path.join(scriptsDir, scripts.postscript), rPostScript)
            .queueCommands([
                    cgen.gen("chmod +x " + rPostScript),
                    cgen.gen("echo 'Executing Postscript'"),
                    cgen.gen(rPostScript),
                    cgen.gen('rm -f ' + rPostScript)
                ]
                .concat(run.endCommands)
            );
        }
        try {
            resolve(await run.exec());
        } catch (err) {
            reject(err);
        }
    });
};

module.exports = {
    exec: exec
};