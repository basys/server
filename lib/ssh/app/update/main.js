const executor = require('./executor');

module.exports = function(app, options) {
    
    const startExec = function() {
        return new Promise(async (resolve, reject) => {
            let configs = app.configs;
            let logFiles = [];
            for(let i=0; i<configs.length; ++i) {
                try {
                    logFiles.push(await executor.exec(app, configs[i], options));
                } catch (err) {
                    return reject(err);
                }
            }
            resolve(logFiles);
        });
    };

    return {
        start: startExec
    };
};