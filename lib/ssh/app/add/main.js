const executor = require('./executor');

module.exports = function(app, options) {
    const pushLog = function(type, msg) {
        msg = new Date() + " :: " + msg;
        if(options.op && options.router && options.router.opLog) options.router.opLog(options.op, type, msg);
        if(options.stream) console.log(msg);
    };

    const startExec = function() {
        return new Promise(async (resolve, reject) => {
            let configs = app.configs;
            let logFiles = [];
            for(let i=0; i<configs.length; ++i) {
                try {
                    pushLog(5, '\n' + 'Setup server ' + (i + 1) + ' of ' + configs.length);
                    logFiles.push(await executor.exec(app, configs[i], options));
                } catch (err) {
                    return reject(err);
                }
            }
            resolve(logFiles);
        });
    };

    return {
        start: startExec
    };
};