/*
 * Ubuntu 18.x
 * Postgresql v10
 */

const DEFAULT_PASSWORD = "qweasdzxc";

const cgen = require('../cgen');
const installCommands = [
    cgen.gen("sudo apt update"),
    cgen.gen("sudo locale-gen"),
    cgen.gen("apt install postgresql postgresql-contrib"),
    cgen.gen("update-rc.d postgresql enable"),
    cgen.gen("sed -i '$ a host all all 0.0.0.0/0 md5' /etc/postgresql/10/main/pg_hba.conf"),
    cgen.gen("sed -i \"s/#listen_addresses = 'localhost'/listen_addresses = '*'/\" /etc/postgresql/10/main/postgresql.conf"),
    cgen.gen("sed -i 's/max_connections = 100/max_connections = 10000/' /etc/postgresql/10/main/postgresql.conf"),
    cgen.gen("sudo -u postgres psql -c \"ALTER USER postgres WITH PASSWORD '" + DEFAULT_PASSWORD + "';\""),
    cgen.gen("service restart postgresql")
];
const uninstallCommands = [
    cgen.gen("sudo apt-get --purge remove postgresql -y")
];

module.exports = {
    installCommands: installCommands,
    uninstallCommands: uninstallCommands
};