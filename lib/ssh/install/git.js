const cgen = require('../cgen');
const installCommands = [
    cgen.gen("sudo apt-get install git -y")
];
const uninstallCommands = [
    cgen.gen("sudo apt-get remove git -y")
];

module.exports = {
    installCommands: installCommands,
    uninstallCommands: uninstallCommands
};