const cgen = require('../cgen');
const installCommands = [
    cgen.gen("sudo apt install build-essential -y"),
    cgen.gen("curl -sL https://deb.nodesource.com/setup_8.x -o ~/node_setup.sh"),
    cgen.gen("sudo bash ~/node_setup.sh -y"),
    cgen.gen("rm ~/node_setup.sh"),
    cgen.gen("sudo apt-get install nodejs -y"),
    cgen.gen("node -v"),
    cgen.gen("npm -v")
];
const uninstallCommands = [
    cgen.gen("sudo apt remove nodejs -y"),
    cgen.gen("sudo apt purge nodejs -y"),
    cgen.gen("sudo apt autoremove -y")
];

module.exports = {
    installCommands: installCommands,
    uninstallCommands: uninstallCommands
};