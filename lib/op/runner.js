const uuid = require('uuid/v4');
const queue = require('./queue');
const appDB = require("../db/apps");
const adder = require('../ssh/app/add/main');
const updater = require('../ssh/app/update/main');

const checkExecute = async function() {
    const ops = queue.getNextOps();
    for(let i=0; i<ops.length; ++i) {
        let op = ops[i];
        switch(op.name) {
            case 'UPDATE':
                try {
                    queue.setRunning(op);
                    await updater(appDB.getApp(op.app), {
                        op: op,
                        router: require('../../ws/cli').router,
                        stream: true
                    }).start();
                    queue.clearRunning(op);
                } catch (err) {
                    queue.clearRunning(op, err);
                }
                break;
            default:
                queue.clearRunning(op, 'Invalid Operation : ' + op.name);
                appDB.deleteOp(op.oid);
            }
    }
    setTimeout(checkExecute, 2500);
};

//Start
checkExecute();

module.exports = {
    genOp: function(type, app) {
        function getMsg(type) {
            switch(type) {
                case 'UPDATE': return 'Queued';
                default: return 'Created';
            }
        };

        return {
            oid: uuid(),
            name: type,
            app: app.name,
            status: 0,
            msg: getMsg(type)
        };
    },

    add: function(app, op) {
        return new Promise(async (resolve, reject) => {
            try {
                if(!op) throw Error('Operation (op) parameter cannot be null');
                resolve(await adder(app, {
                    op: op,
                    router: require('../../ws/cli').router,
                    stream: true
                }).start());
            } catch (err) {
                reject(err);
            }
        });
    },

    update: function(app) {
        queue.queueOp(this.genOp('UPDATE', app));
    }
};