const queues = require('../util/map')();
const running = require('../util/map')();
const appDB = require('../db/apps');

//Re-Init on Launch
const init = function() {
    const apps = appDB.getApps();
    for(let i=0; i<apps.length; ++i) {
        let app = apps[i];
        const ops = appDB.getOps(app.name, true);
        if(ops.length == 0) continue;
        for(let j=0; j<ops.length; ++j) {
            queueOp(ops[j]);
        }
    }
};

const queueOp = function(op) {
    let queue = queues.get(op.app);
    if(!queue) queue = [];
    removeSimilarOps(queue, op);
    appDB.addOp(op);
    queue.push(op);
    queues.set(op.app, queue);
    return op;
};

const removeSimilarOps = function(queue, op) {
    for(let i=0; i<queue.length; ++i) {
        const qop = queue[i];
        if(qop.name == op.name) {
            appDB.deleteOp(qop.oid);
            queue.splice(i--, 1);
        }
    }
};

//Init
init();

//Export
module.exports = {
    queueOp: queueOp,

    nextOp: function(appName) {
        let queue = queues.get(appName);
        if(!queue || queue.length==0) return null;
        return queue.shift();
    },

    getNextOps: function() {
        let out = [];
        let apps = queues.keys();
        for(let i=0; apps.length; ++i) {
            let appName = apps[i];
            if(running.has(appName)) continue;
            let op = this.nextOp(appName);
            if(!op) continue;
            out.push(op);
        }
        return out;
    },

    getQueuedOps: function() {
        const out = [];
        const keys = queues.keys();
        for(let i=0; i<keys.length; ++i) {
            let key = keys[i];
            let ops = queues.get(key);
            if(ops.length==0) continue;
            out.push({
                app: key,
                ops: ops
            });
        }
        return out;
    },

    clearOps: function(appName) {
        queues.delete(appName);
    },

    setRunning: function(op) {
        op.status = 1;
        op.msg = "In progress";
        appDB.updateOp(op);
        running.set(op.app, op);
        return op;
    },

    getRunningOps: function() {
        return running.values();  
    },

    clearRunning: function(op, err) {
        op.status = !err ? 2 : 3;
        op.msg = err ? (err instanceof Object ? JSON.stringify(err) : err) : 'Done';
        appDB.updateOp(op);
        running.delete(op.app);
        return op;
    }
};