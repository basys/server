/*
 * Apps Config
 */
const DB = require('better-sqlite3');
const path = require('path');
const db = new DB(path.join(__dirname, "..", "..", "apps.db"));

/*
 * App Config JSON
 * {
 *      name: str,
 *      dir: str,
 *      gitconfig?: {
 *          service: str,
 *          projectid: ?,
 *          repo: str,
 *          branch: str,
 *          username: str,
 *          password: str,
 *          token?: str
 *      },
 *      install?: [
 *          text
 *      ],
 *      scripts?: {
 *          basyssetup: text,
 *          basysupdate: text,
 *          prescript?: text,
 *          script?: text,
 *          postscript?: text
 *      },
 *      configs: [
 *          {
 *              host: str,
 *              port: int,
 *              username: str,
 *              password: str,
 *              key?: str
 *          }
 *      ],
 *      options?: {
 *          secure?: boolean
 *      }
 * }
 */

//Setup DB
const CREATE_TABLES =
//Apps
"CREATE TABLE IF NOT EXISTS apps (" +
"name TEXT PRIMARY KEY, " +
"gitconfig TEXT, " +
"dir TEXT, " +
"install TEXT, " +
"scripts TEXT, " +
"configs TEXT, " +
"options TEXT" +
"); " +

//Operations
"CREATE TABLE IF NOT EXISTS ops (" +
"oid TEXT NOT NULL PRIMARY KEY, " +
"name TEXT NOT NULL, " +
"app TEXT NOT NULL, " +
"status TEXT NOT NULL, " +
"msg TEXT, " +
"tin INTEGER NOT NULL, " +
"tlast INTEGER NOT NULL, " +
"FOREIGN KEY (app) REFERENCES apps(name)" +
"); "
;
db.exec(CREATE_TABLES);

//Operation Status codes
const OPSTAT = {
    QUEUED: 0,
    PROCESSING: 1,
    SUCCESS: 2,
    FAILED: 3
};

exports = module.exports = {
    //Apps
    //Add
    addApp: function(app) {
        if(!app) throw Error("Invalid parameter(s) passed");
        if(this.getApp(app.name)) throw Error("App with this name already exists");
        let stmt = db.prepare("INSERT INTO apps (name, dir, gitconfig, install ,scripts, configs, options) VALUES (?, ?, ?, ?, ?, ?, ?)");
        let info = stmt.run(
            app.name,
            app.dir,
            JSON.stringify(app.gitconfig ? app.gitconfig : {}), 
            JSON.stringify(app.install ? app.install : []),
            JSON.stringify(app.scripts ? app.scripts : {}),
            JSON.stringify(app.configs ? app.configs : []),
            JSON.stringify(app.options ? app.options : {})
        );
        return app;
    },

    //Update
    updateApp: function(oldApp, app) {
        if(!oldApp || !app) throw Error("Invalid parameter(s) passed");
        if(!this.getApp(oldApp.name)) throw Error("App does not exist");
        let stmt = db.prepare("UPDATE apps SET name=?, dir=?, gitconfig=?, install=?, scripts=?, configs=?, options=? WHERE name=?");
        let info = stmt.run(
            app.name,
            app.dir,
            JSON.stringify(app.gitconfig ? app.gitconfig : {}),
            JSON.stringify(app.install ? app.install : []),
            JSON.stringify(app.scripts ? app.scripts : {}),
            JSON.stringify(app.configs ? app.configs : []),
            JSON.stringify(app.options ? app.options : {}),
            oldapp.name
        );
        return app;
    },

    //Delete
    deleteApp: function(appName) {
        if(!appName) throw Error("Invalid parameter(s) passed");
        if(!this.getApp(appName)) throw Error('App does not exist');
        let stmt = db.prepare("DELETE FROM apps WHERE name=?");
        let info = stmt.run(appName);
        return true;
    },

    //Get
    //By Name
    getApp: function(appName) {
        if(!appName) throw Error("Invalid parameter(s) passed");
        let app = db.prepare("SELECT * FROM apps WHERE name=?").get(appName);
        if(!app) return null;
        app.gitconfig = JSON.parse(app.gitconfig);
        app.install = JSON.parse(app.install);
        app.scripts = JSON.parse(app.scripts);
        app.configs = JSON.parse(app.configs);
        app.options = JSON.parse(app.options);
        return app;
    },

    //All
    getApps: function() {
        let apps = [];
        let appNames = db.prepare('SELECT name FROM apps').all();
        for(let i=0; i<appNames.length; ++i) {
            let app = this.getApp(appNames[i]);
            if(!app) continue;
            apps.push(app);
        }
        return apps;
    },

    //Operations
    OPSTAT: OPSTAT,

    //Add
    addOp: function(op) {
        if(!op) throw Error("Invalid parameter(s) passed");
        let stmt = db.prepare('INSERT INTO ops (oid, name, app, status, msg, tin, tlast) VALUES (?, ?, ?, ? ,?, ?, ?)');
        let info = stmt.run(op.oid, op.name, op.app, op.status, op.msg, Date.now(), -1);
        return op;
    },

    //Update
    updateOp: function(op) {
        if(!op) throw Error("Invalid parameter(s) passed");
        if(!this.getOp(op.oid)) throw Error("Operation with this id is not found");
        let stmt = db.prepare('UPDATE ops SET status=?, msg=?, tlast=? WHERE oid=?');
        let info = stmt.run(op.status, Date.now(), op.msg, op.oid);
        return op;
    },

    //Delete
    deleteOp: function(oid) {
        if(!oid) throw Error("Invalid parameter(s) passed");
        if(!this.getOp(oid)) throw Error("Operation with this id is not found :: ID : " + oid);
        let stmt = db.prepare("DELETE FROM ops WHERE oid=?");
        let info = stmt.run(oid);
        return true;
    },

    //Get
    //By Id
    getOp: function(oid) {
        if(!oid) throw Error("Invalid parameter(s) passed");
        return db.prepare("SELECT * FROM ops WHERE oid=?").get(oid);
    },

    //By App
    getOps: function(appName, ignoreFailed) {
        if(!appName) throw Error("Invalid parameter(s) passed");
        if(ignoreFailed) {
            return db.prepare("SELECT * FROM ops WHERE app=? AND status<? ORDER BY tin").all(appName, OPSTAT.SUCCESS);
        } else {
            return db.prepare("SELECT * FROM ops WHERE app=? ORDER BY tin").all(appName);
        }
    },

    //All
    getAllOps: function() {
        return db.prepare("SELECT * FROM ops ORDER BY tin DESC").all();
    }
};
