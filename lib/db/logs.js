/*
 * Logs
 */
const DB = require('better-sqlite3');
const uuid = require("uuid/v4");
const path = require('path');
const db = new DB(path.join(__dirname, "..", "..", "logs.db"));

//Setup DB
const CREATE_TABLES =
"CREATE TABLE IF NOT EXISTS logs (" +
"lid TEXT PRIMARY KEY, " +
"logs TEXT NOT NULL" +
"); "
;
db.exec(CREATE_TABLES);

exports = module.exports = {
    //Logs
    //Add
    addLog: function(message) {
        try {
            if(message instanceof Object) message = JSON.stringify(message);
            let stmt = db.prepare("INSERT INTO logs (lid, log) VALUES (?, ?)");
            let info = stmt.run(uuid(), message);
            return true;
        } catch (e) {
            return false;
        }
    },

    //Delete
    deleteLog: function(id) {
        try {
            let stmt = db.prepare("DELETE FROM logs WHERE lid=?");
            let info = stmt.run(id);
            return true;
        } catch (e) {
            return false;
        }
    },

    //Get
    getLog: function(id) {
        if(!id) throw Error("Invalid parameter(s) passed");
        return db.prepare("SELECT * FROM logs WHERE lid=?").get(id);
    },

    getLogs: function() {
        return db.prepare('SELECT * FROM logs').all();
    }
};
