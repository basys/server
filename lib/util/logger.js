const fs = require('fs');
const path = require('path');
const logsDir = path.join(__dirname, "..", "..", "logs");

if(!fs.existsSync(logsDir)) fs.mkdirSync(logsDir);

module.exports = function(name) {
    let logFile = null;
    let logFilePath = null;
    let fileStream = null;

    return {
        start() {
            logFile = (name ? name + "-" : "") + new Date() + ".log";
            logFilePath = path.join(logsDir, logFile);
            fileStream = fs.createWriteStream(logFilePath);
            return this;
        },
        
        write(chunk) {
            if(!fileStream) return false;
            fileStream.write(chunk);
            return true;
        },
        
        stop() {
            return new Promise((resolve, reject) => {
                if(fileStream) {
                    fileStream.end(() => {
                        resolve(logFilePath);
                    });
                    fileStream = null;
                } else {
                    reject();
                }
            });
        }
    };
};