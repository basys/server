const map = new Map();

module.exports = function(set, del) {
    return {
        get: function(key) {
            return map.get(key);
        },
        set: function(key, value) {
            map.set(key, value);
            if(set) set(key, value);
        },
        delete: function(key) {
            if(!map.has(key)) return false;
            let val = map.get(key);
            map.delete(key);
            if(del) del(key, val);
        },
        has: function(key) {
            return map.has(key);
        },
        keys: function() {
            let out = [];
            for(var key in map.keys()) out.push(key);
            return out;
        },
        values: function() {
            let out = [];
            for(var value in map.values()) out.push(value);
            return out;
        }
    };
};