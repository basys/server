const crypto = require("crypto");
const rsa = require("node-rsa");

function generateKeyPair() {
    var rsai = new rsa({
        b: 4096
    });
    return {
        public: rsai.exportKey('pkcs8-public-pem'),
        private: rsai.exportKey('pkcs8-private')
    };
}

function encrypt(key, data) {
    var rsai = new rsa();
    rsai.importKey(key, 'pkcs8-public-pem');
    return rsai.encrypt(data, 'base64', 'utf8');
}

function decrypt(key, data) {
    var rsai = new rsa();
    rsai.importKey(key, 'pkcs8-private');
    return rsai.decrypt(data, 'utf8', 'base64');
}

function sign(key, data) {
    var rsai = new rsa();
    rsai.importKey(key, 'pkcs8-private');
    return rsai.sign(data, 'base64', 'utf8');
}

function verify(key, value, signature) {
    var rsai = new rsa();
    rsai.importKey(key, 'pkcs8-public-pem');
    return rsai.verify(value, signature, 'utf8', 'base64');
}

function randomBytes(noBytes) {
    return crypto.randomBytes(noBytes).toString('hex');
}

module.exports = {
    randomBytes: randomBytes,
    generateKeyPair: generateKeyPair,
    encrypt: encrypt,
    decrypt: decrypt,
    sign: sign,
    verify: verify
};

