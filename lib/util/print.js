const chalk = require('chalk');
let debug = false;

const p = function(options, ...args) {
    //Defaults
    let msg = "";
    let color = "#FFFFFF";
    let separator = "\n";

    //Modify according to Options
    if(options) {
        switch(options.type) {
            case 'special': color = "#044AEA"; break;
            case 'success': color = "#67C23A"; break;
            case 'warning': color = "#E6A23C"; break;
            case 'error': color = "#F56C6C"; break;
            case 'info': color = "#FFFFFF"; break;
            case 'input': color = "#409EFF"; break;
            case 'title': color = "#C900FF"; break;
            default: color = "#FFFFFF";
        }
        if(options.color) color = options.color;
        if(options.separator) separator = options.separator;
        if(options.timestamp == true) msg += new Date() + " : ";
    }
    //Print
    for(let i=0; i<args.length; ++i) {
        let arg = args[i];
        if(!arg) continue;
        msg += (arg instanceof Object ? (arg instanceof Error && debug ? arg.stack : JSON.stringify(arg)) : arg) + (i==args.length-1 ? "" : separator);
    }
    console.log(chalk.hex(color).visible(msg));
};

const sp = function(options, ...args) {
    if(!options) options  = {};
    options.type = 'special';
    args.unshift(options);
    p.apply(this, args);
};

const t = function(options, ...args) {
    if(!options) options  = {};
    options.type = 'title';
    args.unshift(options);
    p.apply(this, args);
};

const s = function(options, ...args) {
    if(!options) options  = {};
    options.type = 'success';
    args.unshift(options);
    p.apply(this, args);
};

const w = function(options, ...args) {
    if(!options) options  = {};
    options.type = 'warning';
    args.unshift(options);
    p.apply(this, args);
};

const e = function(options, ...args) {
    if(!options) options  = {};
    options.type = 'error';
    args.unshift(options);
    p.apply(this, args);
};

const input = function(options, ...args) {
    if(!options) options  = {};
    options.type = 'input';
    args.unshift(options);
    p.apply(this, args);
};

exports = module.exports = {
    p: p,
    s: s,
    w: w,
    e: e,
    in: input,
    t: t,
    sp: sp,
    setDebug: function(val) {
        debug = val;
    }
};