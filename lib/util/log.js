//Set Uncaught Exception handler (I don't want to kill the process)
process.on('uncaughtException', function(err) {
    _LOG(true, "UCE", "Name : " + err.name + "\nMessage : " + err.message + "\nStack Trace : " + err.stack);
});

//Tag
let NAME = 'Unknown';

//Export
module.exports = function(name) {
    //Set Name
    if(name) NAME = name;
    else if(NAME) name = NAME;

    //Attach function
    this._LOG = function(isError, ...args) {
        let errorMessage = new Date() + " :: " + name;
        for(var i=0; i<args.length; ++i) errorMessage += " :: " + args[i];
        if(isError) {
            for(let i=0; i<args.length; ++i) {
                if(args[i]!=null && args[i].stack) {
                    errorMessage += "\nSTACK TRACE:\n" + args[i].stack;
                }
            }
        }
        console.log(errorMessage);
    };
};